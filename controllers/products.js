const { prisma } = require("../prisma/prisma-client");
const fetch = import('node-fetch');
const { json } = require("../helpers/json");
const addProductsToDatabase = async (productsInfo) => {
  try {
    for (const productInfoItem of productsInfo) {
      const pureProductId = +productInfoItem.id.replace(/^gid:\/\/shopify\/Product\//, '')
      const existingProduct = await prisma.product.findUnique({ where: { id: pureProductId } });
      if (!existingProduct) {
        console.log(productInfoItem.images.edges);
        prisma.product.create({
          data: {
            id: pureProductId,
            title: productInfoItem.title,
            bodyHtml: productInfoItem.bodyHtml,
            images: {
              create: productInfoItem.images.edges.map(image => ({
                id: +image.node.id.replace(/^gid:\/\/shopify\/ProductImage\//, ''),
                originalSrc: image.node.originalSrc,
                width: image.node.width,
                height: image.node.height
              })),
            },
          }
        }).catch(error => {
          console.error('Error creating new product:', error);
        });;
        console.log(`Product with ID ${productInfoItem.id} added to the database.`);
      } else {
        console.log(`Product with ID ${productInfoItem.id} already exists in the database.`);
      }
    }
  } catch (error) {
    console.error('Error adding products to the database:', error);
  }
};
const getProducts = async (req, res) => {
  try {
    const { pageCursor = null } = req.query;
    const graphqlEndpoint = `${process.env.shop_name}/admin/api/2023-10/graphql.json`;
    const query = `
    {
      products(first: 5 ${(pageCursor === null) ? '' : ',after: "' + pageCursor + '"'}) {
          pageInfo {
            hasNextPage
            endCursor
          }
          edges {
            node {
              id
              title
              bodyHtml
              images(first: 1) {
                edges {
                  node {
                    id,
                    originalSrc,
                    width,
                    height
                  }
                }
              }
            }
          }
      }
    }
    `;
    const { default: _fetch } = await fetch
    const response = await _fetch(graphqlEndpoint, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'X-Shopify-Access-Token': process.env.access_token,
      },
      body: JSON.stringify({ query }),
    });

    const data = await response.json();
    const productsData = data.data.products.edges;
    const pageInfo = data.data.products.pageInfo;
    const productsInfo = productsData.map(edge => edge.node);
    await addProductsToDatabase(productsInfo);

    req.query.pageCursor = pageInfo.endCursor;
    if (pageInfo.hasNextPage) {
      await getProducts(req, res);
    }
    else {
      try {
        const products = await prisma.product.findMany({
          include: {
            images: true
          }
        });
        res.status(200).type("json").send(json(products));
      } catch (error) {
        console.error('Error fetching products:', error);
        res.status(500).json({ error: 'Internal Server Error' });
      }
    }
  } catch (error) {
    console.error('Error fetching products:', error);
    res.status(500).json({ error: 'Internal Server Error' });
  }
}

module.exports = {
  getProducts
}