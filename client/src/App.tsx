import React from 'react';
import './App.css';
import Layout from '../src/app/layout/Layout';
import { Products } from './app/products';

function App() {
  return (
    <div className="App">
      <Layout title="Shopify products" footerText='Sidachenko Michael, 2014'>
          <Products/>
      </Layout>
    </div>
  );
}

export default App;
