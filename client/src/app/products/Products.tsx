import { Row, Col, Card } from 'antd';
import useImage from 'use-image';
import { Stage, Layer, Image } from 'react-konva';
import { Product } from './Products.types';
import React, { useEffect, useRef } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { fetchProducts } from '../../features/products/productSlice';
import { RootState } from '../store';
interface ProductProps {
  
}

interface ImageProductProps {
  imageSrc: string,
  width: number
}

const calculateOffsetX = (cardRef: React.RefObject<HTMLDivElement>, imageWidth: number): number => {
  const cardWidth = cardRef.current?.getBoundingClientRect()?.width ?? 0;
  return (cardWidth - imageWidth) / 2;
};

const ProductImage = ({ imageSrc, cardRef, width }: ImageProductProps & { cardRef: React.RefObject<HTMLDivElement> }) => {
  const [image] = useImage(imageSrc);
  console.log("imageWidth: ", width)
  const offsetX = calculateOffsetX(cardRef, width);

  return (
    <Image
      scaleX={0.8}
      scaleY={0.8}
      image={image}
    />
  );
};

export default function Products(props: ProductProps) {
  const dispatch = useDispatch();
  const products = useSelector((state: RootState) => state.products);
  const loading = useSelector((state: RootState) => state.loading);
  const error:  string | null  = useSelector((state: RootState) => state.error) as string | null;

  useEffect(() => {
    dispatch(fetchProducts() as any);
  }, [dispatch]);

  const cardRef = useRef<HTMLDivElement>(null);

  if (loading) {
    return <div>Loading...</div>;
  }

  if (error) {
    return <div>Error: {error}</div>;
  }
  return (
    <Row gutter={[16, 16]}>
          {products.map((product: Product) => (
      <Col key={product.id} xs={24} sm={12} md={8}>
        <Card ref={cardRef}>
           <Stage className="overflow-hidden"  width={cardRef.current?.getBoundingClientRect()?.width ?? 0}
                height={(cardRef.current?.getBoundingClientRect()?.height ?? 0) / 2}>
            <Layer>
              <ProductImage width={product.images[0].width} imageSrc={product.images[0].originalSrc} cardRef={cardRef}/>
            </Layer>
          </Stage>
          <Card.Meta className="text-2xl !mb-1" title={product.title} />
          <div dangerouslySetInnerHTML={{ __html: product.bodyHtml }}></div>
        </Card>
      </Col>
    ))}
        </Row>
  );
}