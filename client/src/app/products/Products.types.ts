export interface Product {
  id: number;
  title: string;
  bodyHtml: string;
  images: Image[];
  createdAt: Date;
}

export interface Image {
  id: number;
  originalSrc: string;
  productId: number;
  width: number;
  height: number;
}