import { Layout as AntLayout, Flex, Typography } from 'antd';
const { Header, Content, Footer } = AntLayout;
const { Title } = Typography;
interface Props {
  title: string,
  children: React.ReactNode
  footerText: string
}

const contentStyle: React.CSSProperties = {
  minHeight: "auto",
};

export default function Layout({children, title, footerText}: Props) {
  return (
    <Flex className="h-screen" vertical>
      <Header className="bg-white">
        <Title className="!text-lime-500 !text-3xl font-bold my-2">{title}</Title>
      </Header>
      <Content className='mb-[2rem]' style={contentStyle}>
        { children }
      </Content>
      <Footer>{footerText}</Footer>
    </Flex>
  );
}